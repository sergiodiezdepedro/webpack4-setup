const css = require('./app.scss');
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <p className="parrafo-react">Este párrafo ha sido generado con React</p>,
  document.getElementById('root')
);