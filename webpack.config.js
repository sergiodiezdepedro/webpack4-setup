const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const glob = require('glob-all');
const PurifyCSSPlugin = require('purifycss-webpack');

module.exports = {
    mode: 'development',
    // mode: 'production',
    entry: {
        app: './src/app.js',
        about: './src/about.js'
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [{
                test: /\.scss$/,
                loaders: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.pug$/,
                use: [{
                        loader: 'html-loader'
                    },
                    {
                        loader: 'pug-html-loader',
                        options: {
                            pretty: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif|svg|jpeg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: './img/[name].[ext]',
                        // outputPath: 'img/'
                    }
                }]
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        // port: 9000,
        stats: 'errors-only',
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Proyecto con Webpack',
            excludeChunks: ['about'],
            template: 'src/index.ejs'
        }),
        new HtmlWebpackPlugin({
            filename: 'about.html',
            title: 'Sobre nosotros',
            chunks: ['about'],
            template: 'src/about.pug'
        }),
        new MiniCssExtractPlugin({
            filename: "app.css",
        }),
        new PurifyCSSPlugin({
            paths: glob.sync(
                [
                    path.join(__dirname, 'src/*.html'),
                    path.join(__dirname, 'src/*.pug'),
                    path.join(__dirname, 'src/includes/*.pug'),
                    path.join(__dirname, 'src/*.ejs'),
                    path.join(__dirname, 'src/*.js')
                ]),
            minimize: true
        })
    ]
};