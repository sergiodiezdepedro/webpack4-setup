# Webpack

[Sitio Oficial](https://webpack.js.org/)

## Scripts

En el `package.json` se definen dos modos de ejecución:

- **dev**: mediante el servidor de producción se lanza el navegador y se refresca el mismo según se realizan cambios en los archivos. No se generan los archivos para producción.

- **prod**: Webpack empaqueta todo y genera los archivos necesarios para el despliegue de la aplicación en un servidor de producción. Todo ello se dirige a un directorio de nombre convencional, que en este ejemplo denomino como `dist/`.   

## Loaders

[Loaders](https://webpack.js.org/concepts/loaders/) son transformaciones que se aplican al código fuente de un módulo. Por decirlo de otro modo, son como las *tareas* de otras herramientas de desarrollo, que proporcionan facilidades en las etapas de desarrollo front-end. 

## Servidor de desarrollo

Webpack dispone de un servidor de desarrollo que refresca el navegador al realizar cambios en los archivos. Se denomina [webpack-dev-server](https://webpack.js.org/configuration/dev-server/).

En el momento de desplegar este proyecto este servidor lanza una advertencia que aconseja definir el [modo](https://webpack.js.org/concepts/mode/), **development** o **production**. 

## Generar dinámicamente los archivos HTML

Mediante el plugin **html-webpack**.

[Sitio](https://github.com/jantimon/html-webpack-plugin)

Para personalizar la página web que genera automáticamente el plugin se puede utilizar un **template**, que tiene la extensión `.ejs`. En este proyecto, se encuentra en la carpeta `src/`.

Obviamente, se pueden crear varios archivos html distintos, con su correspondiente template si así se desea, simplemente mediante la repetición del uso del plugin en el archivo de configuración de Webpack.

## CSS con css-loader y style-loader

[Sitio de css-loader](https://github.com/webpack-contrib/css-loader)

[Sitio de style-loader](https://github.com/webpack-contrib/style-loader)

## Integración de Sass

Mediante [sass-loader](https://github.com/webpack-contrib/sass-loader). Se necesita como dependencia [node-sass](https://github.com/sass/node-sass). 

## Extracción de los estilos a un archivo css 

Para ello se utilizaba [extract-text-webpack-plugin](https://github.com/webpack-contrib/extract-text-webpack-plugin), pero este plugin, en el momento en el que escribo estas palabras, no es compatible con Webpack 4. 

Sin embargo, mediante otro plugin, [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin), se puede realizar satisfactoriamente la tarea.  

## React y Babel

Se puede implementar el uso de [React](https://reactjs.org/) de forma manual en el entorno Webpack de forma sencilla. Y, naturalmente, el uso de [Babel](https://babeljs.io/) para convertir el código javascript en la versión interpretable por cualquier navegador.

## Limpiar carpeta de distribución

Para asegurarse de que los archivos generados por el script *prod* son los presentes en la carpeta `dist/` se utiliza un paquete node llamado **rimraf**, que elimina los archivos de una carpeta definida. Así, en el script *prod*, antes de crear los mismos se efectúa un borrado de todo el contenido anterior. 

## Distintos puntos de entrada

Si fuera necesario utilizar dos archivos `.js` diferentes, con sus correspondientes páginas html, se pueden definir puntos de entrada distintos.

## Plantillas HTML con Pug

Se puede utilizar [Pug](https://pugjs.org/) como motor de plantillas HTMl en el ecosistema Webpack. Para ello hay que instalar el propio Pug, y dos loaders, [pug-html-loader](https://github.com/willyelm/pug-html-loader) y [html-loader](https://webpack.js.org/loaders/html-loader/).

Por razones operativas, Pug no es herramienta práctica para introducir en sus archivos sintaxis propia de React.

## Soporte para incluir imágenes

En webpack hay un *loader* que permite incluir imágenes en el entorno de desarrollo que son exportadas adecuadamente en el paquete de producción final. Se denomina [file-loader](https://github.com/webpack-contrib/file-loader), que requiere de cierta configuración para que esa tarea de exportación final siga el comportamiento habitual y deseado. 

## Optimización del CSS

Se puede realizar una labor de limpieza de las clases no utilizadas y que, sin embargo, están presentes en las hojas de estilo de desarrollo. Para ello, se recurre al plugin [purifycss-webpack](https://github.com/webpack-contrib/purifycss-webpack), que tiene como dependencias el *paquete padre* **purify-css** y **glob-all**, utilidad esta que permite pasar un array de rutas y tipos de archivos diferentes, que son los que se escanean en busca de las clases realmente utilizadas. Es decir, que mediante este recurso se escrutan archivos tales como .html, .pug, .ejs, .js, etc.

Gracias una simple opción del plugin, el CSS final de producción queda minificado. 

Obviamente, esta optimización de las hojas de estilo tiene auténtico sentido en producción, vale decir, para el acabado final.
